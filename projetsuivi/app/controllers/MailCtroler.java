package controllers;

import javax.inject.Inject;

import Serivice.Mail;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Result;

public class MailCtroler  extends Controller{
	Mail mail;
	FormFactory dataForm;
	@Inject
	public MailCtroler(Mail mail,FormFactory dataForm){
		super();
		this.mail=mail;
		this.dataForm=dataForm;
		}
	public Result adress(Request request){
		return ok(views.html.mail.render(request));
		}
	public Result envoiMail(Request request){
		String toEmail = dataForm.form().bindFromRequest(request).get("toEmail");
		String objetMail = dataForm.form().bindFromRequest(request).get("objet");
		String contentEmail = dataForm.form().bindFromRequest(request).get("contenu");
		try{

		String resultat=Mail.sendMail(toEmail,objetMail,contentEmail);
		if(resultat.equals("ok")){
			return ok(views.html.mail.render(request));
			
		}
		else{
			return ok(views.html.mail.render(request));
		}
		
	}

	catch(Exception e){
		System.out.println(e.getMessage());
	}
		return ok(views.html.mail.render(request));
	}
	
	

}
