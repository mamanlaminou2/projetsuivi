package controllers;

import java.util.List;

import com.google.inject.Inject;

import Serivice.ProgrammeService;
import Serivice.RapportService;
import models.tables.pojos.Programme;
import models.tables.pojos.Rapport;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.Request;

public class RapportCtroler extends Controller {
	FormFactory dataForm;
	RapportService RapportService;
	@Inject
	public RapportCtroler(FormFactory dataForm, RapportService RapportService){
		this.dataForm =dataForm;
		this.RapportService =RapportService;
	}
	
	public Result formulaireRapport(Request request) {
		return ok(views.html.RapportView.render(request));
	}
	

	public Result listeRapport(Request request) {
		List<Rapport> ps=RapportService.listRapport();
		return ok(views.html.Rapportliste.render(ps, request));
	}
	
	
	public  Result ajoutRapport(Request request) {
    	
		 Form<Rapport> form= dataForm.form(Rapport.class).bindFromRequest(request);
		 Rapport pro = form.get();
		 
		 //pro.setCreerPar("Login");
		 //pro.setDelete(0);
		
		 
		 if(RapportService.isExistNumRapprot(pro.getNumRapport())){
			 return redirect(routes.RapportCtroler.formulaireRapport())
					 .flashing("error", "Rapport avec numro "+pro.getNumRapport()+" existe déja");
		 }
		 
		 
//		 if(RapportService.isLibelleExist(pro.getLibelleRapport())){
//			 return redirect(routes.RapportCtrole.formulaireRapport())
//					 .flashing("error", "Rapport avec libelle "+pro.getLibelleRapport()+" existe déja");
//		 }
		 
		 String resultat=RapportService.saveRapport(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.RapportCtroler.listeRapport())
				 .flashing("success", "Rapport ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.RapportCtroler.formulaireRapport())
				 .flashing("error", "Rapport non ajouté");
	 }
	 
  }
	
	public  Result modifRapport(Request request) {
    	
		 Form<Rapport> form= dataForm.form(Rapport.class).bindFromRequest(request);
		 Rapport pro = form.get();
		 
		// pro.setCreerPar("Login");
		
		 
		 
//		 
//		 if(RapportService.isLibelleExist(pro.getLibelleRapport())){
//			 return redirect(routes.RapportCtrole.formulaireRapport())
//					 .flashing("error", "Rapport avec libelle "+pro.getLibelleRapport()+" existe déja");
//		 }
//		 
		 
		 
		 String resultat=RapportService.editRapport(pro);
		 
		 
	 if(resultat.equals("ok")) {
		 return redirect(routes.RapportCtroler.listeRapport())
				 .flashing("success", "Rapport modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.RapportCtroler.formulaireRapport())
				 .flashing("error", "Rapport non modifié");
	 }
	 
  }
	
	public Result editForm(String numRapport,Request request) {
    	
    	Rapport p=RapportService.byNumrapport(numRapport);
    	
    	return ok(views.html.RapportModif.render(p,request));
    }
	
	public Result deleteRapport(String numRapport,Request request) {
    	
		 String resultat=RapportService.delRapport(numRapport);
		 
		 
		 if(resultat.equals("ok")) {
			 return redirect(routes.RapportCtroler.listeRapport())
					 .flashing("success", "Rapport supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.RapportCtroler.formulaireRapport())
					 .flashing("error", "Rapport non supprimé");
		 }
		 
	    }
	
	 public Result ProModif(Request request) {
	    	Form <Rapport> form= dataForm.form(Rapport.class).bindFromRequest(request);
	    	Rapport misepro = form.get();
	    	if(RapportService.editRapport(misepro).equals("ok")) {
	    		
	    		System.out.print("Modification effectuée avec succes!");
	    		}
	    	else {
	    		System.out.print("Modification non effectuée");
	    	}
	    	return ok(views.html.Rapportliste.render(RapportService.listRapport(), request));
	  }

}
