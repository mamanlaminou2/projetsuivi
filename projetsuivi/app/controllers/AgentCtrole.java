package controllers;

import com.google.inject.Inject;

import Serivice.AgentService;
import  Serivice.ProfilService;
import Serivice.StructureService;
import models.tables.pojos.*;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Result;
import java.util.List;
import play.data.Form;

public class AgentCtrole extends Controller {
	FormFactory dataForm;
	AgentService agentService;
	ProfilService profilservice;
	StructureService structureService;
	@Inject
	public AgentCtrole(FormFactory dataForm, AgentService agentService,ProfilService profilservice,StructureService structureService){
		this.dataForm =dataForm;
		this.agentService =agentService;
		this.profilservice=profilservice;
		this.structureService=structureService;
	}
	
	public Result AgentView(Request request) {
		List<Profile> ps=profilservice.getAll();
		List<Structure> sp=structureService.listStructure();

		return ok(views.html.AgentView.render(ps,sp,request));
	}
	

	public Result Agentliste(Request request) {
		List<Agent> ps=agentService.getAll();
		return ok(views.html.Agentliste.render(ps, request));
	}
	
	
	public  Result ajoutAgent(Request request) {
    	
		 Form<Agent> form= dataForm.form(Agent.class).bindFromRequest(request);
		 Agent pro = form.get();
		 
		 //pro.setCreerPar("Login");
		 //pro.setDelete(0);
		
		 
		 if(agentService.isNumExist(pro.getMatAgent())){
			 return redirect(routes.AgentCtrole.AgentView())
					 .flashing("error", "Agent avec numro "+pro.getMatAgent()+" existe déja");
		 }
		 
		 
		 if(agentService.isLibelleExist(pro.getNom())){
			 return redirect(routes.AgentCtrole.AgentView())
					 .flashing("error", "Agent avec libelle "+pro.getNom()+" existe déja");
		 }
		 
		 String resultat=agentService.savePro(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.AgentCtrole.Agentliste())
				 .flashing("success", "Agent ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.AgentCtrole.AgentView())
				 .flashing("error", "Agent non ajouté");
	 }
	 
  }
	
	public  Result modifAgent(Request request) {
    	
		 Form<Agent> form= dataForm.form(Agent.class).bindFromRequest(request);
		 Agent pro = form.get();
		 
		 //pro.setCreerPar("Login");
		
		 
		 
		 
		 if(agentService.isLibelleExist(pro.getNom())){
			 return redirect(routes.AgentCtrole.AgentView())
					 .flashing("error", "Agent avec libelle "+pro.getNom()+" existe déja");
		 }
		 
		 
		 
		 String resultat=agentService.editAgent(pro);
		 
		 
	 if(resultat.equals("ok")) {
		 return redirect(routes.AgentCtrole.Agentliste())
				 .flashing("success", "Agent modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.AgentCtrole.AgentView())
				 .flashing("error", "Agent non modifié");
	 }
	 
  }
	
	public Result editAgent(String matAgent,Request request) {
    	
    	Agent p=agentService.getByNum(matAgent);
    	
    	return ok(views.html.AgentModif.render(p,request));
    }
	
	public Result delAgent(String matAgent,Request request) {
    	
		 String resultat=agentService.delAgent(matAgent);
		 
		 
		 if(resultat.equals("ok")) {
			 return redirect(routes.AgentCtrole.Agentliste())
					 .flashing("success", "Agent supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.AgentCtrole.AgentView())
					 .flashing("error", "Agent non supprimé");
		 }
		 
	    }
	
	 public Result editAgent(Request request) {
	    	Form <Agent> form= dataForm.form(Agent.class).bindFromRequest(request);
	    	Agent misepro = form.get();
	    	if(agentService.editAgent(misepro).equals("ok")) {
	    		
	    		System.out.print("Modification effectuée avec succes!");
	    		}
	    	else {
	    		System.out.print("Modification non effectuée");
	    	}
	    	return ok(views.html.Agentliste.render(agentService.getAll(), request));
	  }

}

