package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.inject.Inject;
import Serivice.ActiviteService;
import Serivice.NatureService;
import Serivice.TacheService;
import models.tables.pojos.Activite;
import models.tables.pojos.Natureeconomique;
import models.tables.pojos.Programme;
import models.tables.pojos.Tache;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Files.TemporaryFile;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Http.Request;

public class TacheCtroler extends Controller {
	
	FormFactory dataForm;
	TacheService tacheService;
	ActiviteService activiteService;
	NatureService natureService;
	@Inject
	public TacheCtroler(FormFactory dataForm, TacheService tacheService,ActiviteService activiteService,NatureService natureService){
		this.dataForm =dataForm;
		this.tacheService =tacheService;
		this.activiteService =activiteService;
		this.natureService=natureService;
	}
	
	public Result formulaireTache(Request request) {
		List<Activite> lt=tacheService.listActivite();
		List<Natureeconomique> ln=natureService.getAll();
		return ok(views.html.TacheView.render(lt,ln, request));
	}
	
	public Result formFile(Request request) {
		return ok(views.html.FileTache.render(request));
	}
	
	public Result traitement(Request request) {
		 Http.MultipartFormData<TemporaryFile> body = request.body().asMultipartFormData();
		    Http.MultipartFormData.FilePart<TemporaryFile> picture = body.getFile("fichier");
		    List<Tache> lt=new ArrayList<>();
		    if (picture != null) {
		      String fileName = picture.getFilename();
		      long fileSize = picture.getFileSize();
		      String contentType = picture.getContentType();
		      TemporaryFile file = picture.getRef();
		      String extension=getExtension(fileName);
		      if(extension.equals("xlsx")){
		    	file.copyTo(Paths.get("C:/Users/user/workspace/projetsuivi/lib/tache.xlsx"), true);
		    	try {
		    		lt=load("C:/Users/user/workspace/projetsuivi/lib/tache.xlsx");
		    		
		    		boolean test=true;
		    		
		    		for (Tache tache : lt) {
						String resultat=tacheService.saveTache(tache);
						if(!resultat.equals("ok")){
							test=false;
						}
					}

		    		 if(test) {
		    			 return redirect(routes.TacheCtroler.listeTache())
		    					 .flashing("success", "Tache ajouté avec success");
		    			 }
		    		 else {
		    			 //System.out.println(resultat);
		    			 return redirect(routes.TacheCtroler.formulaireTache())
		    					 .flashing("error", "Tache non ajouté");
		    		 }
		    		 
		    		 
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
		    	
			    }else{
		    	
		      }
		      
		  }else{
			 
		  }
		return ok(views.html.FileTache.render(request));
	}
	
	
	private String getExtension(String fileName) {
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extension = fileName.substring(i + 1);
		}
		return extension;
	}
	
	
	public List<Tache>  load(String path) {
	      List<Tache> taches=new ArrayList();

	      try {
	    	  FileInputStream fis = new FileInputStream(new File(path));
		      
		      XSSFWorkbook workbook = new XSSFWorkbook(fis);
		      XSSFSheet spreadsheet = workbook.getSheetAt(0);
	    	  int rows=spreadsheet.getLastRowNum();
				int cols=spreadsheet.getRow(1).getLastCellNum();
				
				
				
				for(int r=1;r<=rows;r++)
				{
					
					XSSFRow row=spreadsheet.getRow(r);
					Tache p=new Tache();
					
					for(int c=0;c<cols;c++)
					{
						XSSFCell cell=row.getCell(c);
						
						if(c==0){
							System.out.println(cell.getStringCellValue()+" ---------0");
							p.setIdTache(cell.getStringCellValue());
						}
						else if (c==1) {
							System.out.println(cell.getStringCellValue()+" ---------1");

							p.setLibelleTache(cell.getStringCellValue());
						}
						
						else if (c==2) {
							System.out.println(cell.getStringCellValue()+" ---------2");

							p.setNumActivite(cell.getStringCellValue());

						}
						
						else if (c==3) {
							System.out.println(cell.getStringCellValue()+" ---------3");

							p.setIdNature(cell.getStringCellValue());
						}
						
						else{
							System.out.println(cell.getStringCellValue()+" ---------4");
							
							p.setCreerPar("test");
							p.setCreeLe(LocalDate.now());
							//p.setDelete(0);
						}
						
						//System.out.println(cell.getStringCellValue());
//			            switch (cell.getCellType()) {
//			               case Cell.CELL_TYPE_NUMERIC:
//			                  System.out.print(cell.getNumericCellValue() + " \t\t ");
//			                  break;
//			               
//			               case Cell.CELL_TYPE_STRING:
//			                  System.out.print(
//			                  cell.getStringCellValue() + " \t\t ");
//			                  break;
//			            }

					}
					
					taches.add(p);
				}
				
					System.out.println(taches.size()+" +++++++++"); 
		      fis.close();
		} catch (Exception e) {
			System.out.println(e.getMessage()); 
		}
	      
	      
	    return taches;
	   
	 }
	

	public Result listeTache(Request request) {
		List<Tache> ps=tacheService.listTache();
		return ok(views.html.Tacheliste.render(ps, request));
	}
	
	
	public  Result ajoutTache(Request request) {
    	
		 Form<Tache> form= dataForm.form(Tache.class).bindFromRequest(request);
		 Tache pro = form.get();
		 
		 
		 if(tacheService.isExistNumRapprot(pro.getIdTache())){
			 return redirect(routes.TacheCtroler.formulaireTache())
					 .flashing("error", "Tache avec numro "+pro.getIdTache()+" existe déja");
		 }
		 
		 
//		 if(TacheService.isLibelleExist(pro.getLibelleTache())){
//			 return redirect(routes.TacheCtrole.formulaireTache())
//					 .flashing("error", "Tache avec libelle "+pro.getLibelleTache()+" existe déja");
//		 }
		 
		 String resultat=tacheService.saveTache(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.TacheCtroler.listeTache())
				 .flashing("success", "Tache ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.TacheCtroler.formulaireTache())
				 .flashing("error", "Tache non ajouté");
	 }
	 
  }
	
	public  Result modifTache(Request request) {
    	
		 Form<Tache> form= dataForm.form(Tache.class).bindFromRequest(request);
		 Tache pro = form.get();	 
		 
		 String resultat=tacheService.editTache(pro);
		 
		 
	 if(resultat.equals("ok")) {
		 return redirect(routes.TacheCtroler.listeTache())
				 .flashing("success", "Tache modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.TacheCtroler.formulaireTache())
				 .flashing("error", "Tache non modifié");
	 }
	 
  }
	
	public Result editForm(String numTache,Request request) {
    	
    	Tache p=tacheService.byNumTache(numTache);
    	
    	return ok(views.html.TacheModif.render(p,request));
    }
	
	public Result deleteTache(String numTache,Request request) {
    	
		 String resultat=tacheService.delTache(numTache);
		 
		 
		 if(resultat.equals("ok")) {
			 return redirect(routes.TacheCtroler.listeTache())
					 .flashing("success", "Tache supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.TacheCtroler.formulaireTache())
					 .flashing("error", "Tache non supprimé");
		 }
		 
	    }
	
	 public Result ProModif(Request request) {
	    	Form <Tache> form= dataForm.form(Tache.class).bindFromRequest(request);
	    	Tache misepro = form.get();
	    	if(tacheService.editTache(misepro).equals("ok")) {
	    		
	    		System.out.print("Modification effectuée avec succes!");
	    		}
	    	else {
	    		System.out.print("Modification non effectuée");
	    	}
	    	return ok(views.html.Tacheliste.render(tacheService.listTache(), request));
	  }



}
