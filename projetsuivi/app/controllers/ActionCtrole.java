package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.inject.Inject;

import Serivice.ActionService;
import Serivice.ProgrammeService;
import models.tables.pojos.Action;
import models.tables.pojos.Profile;
import models.tables.pojos.Programme;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Files.TemporaryFile;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Http.Request;

public class ActionCtrole extends Controller{
	FormFactory dataForm;
	ActionService actionService;
	ProgrammeService programmeService;
	
	@Inject
	
	public ActionCtrole(FormFactory dataForm, ActionService actionSercive,ProgrammeService programmeService){
		this.dataForm=dataForm;
		this.actionService=actionSercive;
		this.programmeService=programmeService;
		}
	
	public Result ActionForm(Request request) {
		List<Programme> ps=programmeService.getAll();
		return ok(views.html.ActionForm.render(ps,request));
	}
	public Result formFile(Request request) {
		return ok(views.html.FileAction.render(request));
	}
	
	public Result traitement(Request request) {
		 Http.MultipartFormData<TemporaryFile> body = request.body().asMultipartFormData();
		    Http.MultipartFormData.FilePart<TemporaryFile> picture = body.getFile("fichier");
		    List<Action> la=new ArrayList<>();
		    if (picture != null) {
		      String fileName = picture.getFilename();
		      long fileSize = picture.getFileSize();
		      String contentType = picture.getContentType();
		      TemporaryFile file = picture.getRef();
		      String extension=getExtension(fileName);
		      if(extension.equals("xlsx")){
		    	file.copyTo(Paths.get("C:/Users/user/workspace/projetsuivi/lib/action.xlsx"), true);
		    	try {
		    		la=load("C:/Users/user/workspace/projetsuivi/lib/action.xlsx");
		    		
		    		boolean test=true;
		    		
		    		for (Action action : la) {
						String resultat=actionService.saveAct(action);
						if(!resultat.equals("ok")){
							test=false;
							System.out.println("action with error****************"+action);
						}
					}

		    		 if(test) {
		    			 return redirect(routes.ActionCtrole.Actionlist())
		    					.flashing("success", "Action ajouté avec success");
		    			 }
		    		 else {
		    			 //System.out.println(resultat);
		    			 return redirect(routes.ActionCtrole.ActionForm())
		    					 .flashing("error", "Action non ajouté");
		    		 }
		    		 
		    		 
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
		    	
			    }else{
		    	
		      }
		      
		  }else{
			 
		  }
		return ok(views.html.FileAction.render(request));
	}
	
	private String getExtension(String fileName) {
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extension = fileName.substring(i + 1);
		}
		return extension;
	}
 
 public List<Action>  load(String path) {
      List<Action> actions=new ArrayList();

      try {
    	  FileInputStream fis = new FileInputStream(new File(path));
	      
	      XSSFWorkbook workbook = new XSSFWorkbook(fis);
	      XSSFSheet spreadsheet = workbook.getSheetAt(0);
    	  int rows=spreadsheet.getLastRowNum();
			int cols=spreadsheet.getRow(1).getLastCellNum();
			
			
			
			for(int r=1;r<=rows;r++)
			{
				
				XSSFRow row=spreadsheet.getRow(r);
				Action a=new Action();
				
				for(int c=0;c<cols;c++)
				{
					XSSFCell cell=row.getCell(c);
					
					if(c==0){
						System.out.println(cell.getStringCellValue()+" ---------1");
						a.setIdAction(cell.getStringCellValue());
					}else if (c==1){
						System.out.println(cell.getStringCellValue()+" ---------2");
						a.setLibelleAction(cell.getStringCellValue());	
					   }else{
						System.out.println(cell.getStringCellValue()+" ---------3");
						a.setNumProgramme(cell.getStringCellValue());
						a.setCreerPar("test");
						a.setCreeLe(LocalDate.now());
						//p.setDelete(0);
					}
					
					//System.out.println(cell.getStringCellValue());
//		            switch (cell.getCellType()) {
//		               case Cell.CELL_TYPE_NUMERIC:
//		                  System.out.print(cell.getNumericCellValue() + " \t\t ");
//		                  break;
//		               
//		               case Cell.CELL_TYPE_STRING:
//		                  System.out.print(
//		                  cell.getStringCellValue() + " \t\t ");
//		                  break;
//		            }

				}
				
				actions.add(a);
			}
			
				System.out.println(actions.size()+" +++++++++"); 
	      fis.close();
	} catch (Exception e) {
		System.out.println(e.getMessage()); 
	}
      
      
    return actions;
   
 }
	
	
	public Result Actionlist(Request request) {
		List<Action> ps=actionService.getAll();
		return ok(views.html.Actionliste.render(ps, request));
	}
	
	public  Result ajoutAction(Request request) {
    	
		 Form<Action> form= dataForm.form(Action.class).bindFromRequest(request);
		 Action pro = form.get();
		 
		 pro.setCreerPar("Login");
		 //pro.setDelete(0);
		
		 
		 if(actionService.isIdExist(pro.getIdAction())){
			 return redirect(routes.ActionCtrole.ActionForm())
					 .flashing("error", "Action avec numro "+pro.getIdAction()+" existe déja");
		 }
		 
		 
		 if(actionService.isLibelleExist(pro.getLibelleAction())){
			 return redirect(routes.ActionCtrole.ActionForm())
					 .flashing("error", "Action avec libelle "+pro.getLibelleAction()+" existe déja");
		 }
		 
		 String resultat=actionService.saveAct(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.ActionCtrole.Actionlist())
				 .flashing("success", "Action ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ActionCtrole.ActionForm())
				 .flashing("error", "Action non ajouté");
	 }
	 
 }
	
	
	public Result editAct(String idAction,Request request) {
   	
   	Action p=actionService.getById(idAction);
   	
   	return ok(views.html.ActionModif.render(p,request));
   	
   }
	
	public Result delAct(String IdAction,Request request) {
   	
		 String resultat=actionService.delAct(IdAction);
		 
		 
		 if(resultat.equals("ok")) {
			 return redirect(routes.ActionCtrole.Actionlist())
					 .flashing("success", "Action supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.ActionCtrole.ActionForm())
					 .flashing("error", "Action non supprimé");
		 }

}
	
	
	
	
}
