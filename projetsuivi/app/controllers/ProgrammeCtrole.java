package controllers;

import com.google.inject.Inject;

import Serivice.CallJasperReport;
import Serivice.ProgrammeService;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.data.Form;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;

import play.libs.Files.DelegateTemporaryFile;
import play.libs.Files.TemporaryFile;
import java.io.File;
import java.io.FileInputStream;


import models.tables.pojos.Programme;

public class ProgrammeCtrole extends Controller {
	FormFactory dataForm;
	ProgrammeService programmeService;
	CallJasperReport jasper;
	static XSSFRow row;
	
	@Inject
	public ProgrammeCtrole(FormFactory dataForm, ProgrammeService programmeService, CallJasperReport jasper){
		this.dataForm =dataForm;
		this.programmeService =programmeService;
		this.jasper=jasper;
	}
	
	public Result formulaireProgramme(Request request) {
		return ok(views.html.ProgrammeView.render(request));
	}
	
	public Result formFile(Request request) {
		return ok(views.html.FormFile.render(request));
	}
	
	public Result traitement(Request request) {
		 Http.MultipartFormData<TemporaryFile> body = request.body().asMultipartFormData();
		    Http.MultipartFormData.FilePart<TemporaryFile> picture = body.getFile("fichier");
		    List<Programme> lp=new ArrayList<>();
		    if (picture != null) {
		      String fileName = picture.getFilename();
		      long fileSize = picture.getFileSize();
		      String contentType = picture.getContentType();
		      TemporaryFile file = picture.getRef();
		      String extension=getExtension(fileName);
		      if(extension.equals("xlsx")){
		    	file.copyTo(Paths.get("C:/Users/user/workspace/projetsuivi/lib/programme.xlsx"), true);
		    	try {
		    		lp=load("C:/Users/user/workspace/projetsuivi/lib/programme.xlsx");
		    		
		    		boolean test=true;
		    		
		    		for (Programme programme : lp) {
						String resultat=programmeService.savePro(programme);
						if(!resultat.equals("ok")){
							test=false;
						}
					}

		    		 if(test) {
		    			 return redirect(routes.ProgrammeCtrole.index())
		    					 .flashing("success", "Programme ajouté avec success");
		    			 }
		    		 else {
		    			 //System.out.println(resultat);
		    			 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
		    					 .flashing("error", "Programme non ajouté");
		    		 }
		    		 
		    		 
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
		    	
			    }else{
		    	
		      }
		      
		  }else{
			 
		  }
		return ok(views.html.FormFile.render(request));
	}
	

	public Result index(Request request) {
		List<Programme> ps=programmeService.getAll();
		return ok(views.html.Programmeliste.render(ps, request));
	}
	
	
	public  Result ajoutProgramme(Request request) {
    	
		 Form<Programme> form= dataForm.form(Programme.class).bindFromRequest(request);
		 Programme pro = form.get();
		 
		 pro.setCreerPar("Login");
		 //pro.setDelete(0);
		
		 
		 if(programmeService.isNumExist(pro.getNumProgramme())){
			 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
					 .flashing("error", "Programme avec numro "+pro.getNumProgramme()+" existe déja");
		 }
		 
		 
		 if(programmeService.isLibelleExist(pro.getLibelleProgramme())){
			 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
					 .flashing("error", "Programme avec libelle "+pro.getLibelleProgramme()+" existe déja");
		 }
		 
		 String resultat=programmeService.savePro(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.ProgrammeCtrole.index())
				 .flashing("success", "Programme ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
				 .flashing("error", "Programme non ajouté");
	 }
	 
  }
	
	public  Result modifProgramme(Request request) {
    	
		 Form<Programme> form= dataForm.form(Programme.class).bindFromRequest(request);
		 Programme pro = form.get();
		 
		 pro.setCreerPar("Login");
		
		 
		 
		 
		 if(programmeService.isLibelleExist(pro.getLibelleProgramme())){
			 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
					 .flashing("error", "Programme avec libelle "+pro.getLibelleProgramme()+" existe déja");
		 }
		 
		 
		 
		 String resultat=programmeService.editPro(pro);
		 
		 
	 if(resultat.equals("ok")) {
		 return redirect(routes.ProgrammeCtrole.index())
				 .flashing("success", "Programme modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
				 .flashing("error", "Programme non modifié");
	 }
	 
  }
	
	public Result editForm(String numProgramme,Request request) {
    	
    	Programme p=programmeService.getByNum(numProgramme);
    	
    	return ok(views.html.ProgrammeModif.render(p,request));
    }
	
	public Result deleteProgramme(String numProgramme,Request request) {
    	
		 String resultat=programmeService.delPro(numProgramme);
		 
		 
		 if(resultat.equals("ok")) {
			 return redirect(routes.ProgrammeCtrole.index())
					 .flashing("success", "Programme supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
					 .flashing("error", "Programme non supprimé");
		 }
		 
	    }
	
	 public Result ProModif(Request request) {
	    	Form <Programme> form= dataForm.form(Programme.class).bindFromRequest(request);
	    	Programme misepro = form.get();
	    	if(programmeService.editPro(misepro).equals("ok")) {
	    		
	    		System.out.print("Modification effectuée avec succes!");
	    		}
	    	else {
	    		System.out.print("Modification non effectuée");
	    	}
	    	return ok(views.html.Programmeliste.render(programmeService.getAll(), request));
	  }
	 
	 
	 private String getExtension(String fileName) {
			String extension = "";

			int i = fileName.lastIndexOf('.');
			if (i > 0) {
				extension = fileName.substring(i + 1);
			}
			return extension;
		}
	 
	 public List<Programme>  load(String path) {
	      List<Programme> programmes=new ArrayList();

	      try {
	    	  FileInputStream fis = new FileInputStream(new File(path));
		      
		      XSSFWorkbook workbook = new XSSFWorkbook(fis);
		      XSSFSheet spreadsheet = workbook.getSheetAt(1);
	    	  int rows=spreadsheet.getLastRowNum();
				int cols=spreadsheet.getRow(1).getLastCellNum();
				
				
				
				for(int r=1;r<=rows;r++)
				{
					
					XSSFRow row=spreadsheet.getRow(r);
					Programme p=new Programme();
					
					for(int c=0;c<cols;c++)
					{
						XSSFCell cell=row.getCell(c);
						
						if(c==0){
							System.out.println(cell.getStringCellValue()+" ---------1");
							p.setNumProgramme(cell.getStringCellValue());}
						 else if (c==1){
                            System.out.println(cell.getStringCellValue()+" ---------2");
							p.setLibelleProgramme(cell.getStringCellValue());
						}else{
							System.out.println(cell.getStringCellValue()+" ---------3");
							p.setAnnee(cell.getStringCellValue());
							p.setCreerPar("test");
							p.setCreeLe(LocalDate.now());
							//p.setDelete(0);
						}
						
						//System.out.println(cell.getStringCellValue());
//			            switch (cell.getCellType()) {
//			               case Cell.CELL_TYPE_NUMERIC:
//			                  System.out.print(cell.getNumericCellValue() + " \t\t ");
//			                  break;
//			               
//			               case Cell.CELL_TYPE_STRING:
//			                  System.out.print(
//			                  cell.getStringCellValue() + " \t\t ");
//			                  break;
//			            }

					}
					
					programmes.add(p);
				}
				
					System.out.println(programmes.size()+" +++++++++"); 
		      fis.close();
		} catch (Exception e) {
			System.out.println(e.getMessage()); 
		}
	      
	      
	    return programmes;
	   
	 }

	 
	 public Result print(Request request) {
			//LocalDateTime now = LocalDateTime.now();
			//String now_string = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm"));
			String templateDir = new File("").getAbsolutePath() + "/reports/spool/";
			
			String reprtName="Programme";
					//dataForm.form().bindFromRequest(request).get("nom");
			
			try {
				
				
					jasper.generateReport(reprtName);

				
				
				return ok(new java.io.File(templateDir +reprtName+".pdf"))
						.flashing("success", "impression ok");

			} catch (Exception e) {
				// flash("error", "erreur impression");
				System.out.println(e.getMessage() + "+++++++--**///////++++++++");
				return redirect(routes.ProgrammeCtrole.formulaireProgramme())
						 .flashing("error", "Programme non modifié");
			}
			
		}

	 


}
