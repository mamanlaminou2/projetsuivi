package controllers;

import java.util.List;

import com.google.inject.Inject;

import Serivice.StructureService;
import models.tables.pojos.Structure;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.Request;

public class StructureCtroler extends Controller {
	
	FormFactory dataForm;
	StructureService StructureService;
	@Inject
	public StructureCtroler(FormFactory dataForm, StructureService StructureService){
		this.dataForm =dataForm;
		this.StructureService =StructureService;
	}
	
	public Result formulaireStructure(Request request) {
		return ok(views.html.StructureView.render(request));
	}
	

	public Result listeStructure(Request request) {
		List<Structure> ps=StructureService.listStructure();
		return ok(views.html.Structureliste.render(ps, request));
	}
	
	
	public  Result ajoutStructure(Request request) {
    	
		 Form<Structure> form= dataForm.form(Structure.class).bindFromRequest(request);
		 Structure pro = form.get();
		 if(StructureService.isExistNumRapprot(pro.getIdStructure())){
			 return redirect(routes.StructureCtroler.formulaireStructure())
					 .flashing("error", "Structure avec numro "+pro.getIdStructure()+" existe déja");
		 }
		 String resultat=StructureService.saveStructure(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.StructureCtroler.listeStructure())
				 .flashing("success", "Structure ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.StructureCtroler.formulaireStructure())
				 .flashing("error", "Structure non ajouté");
	 }
	 
  }
	
	public  Result modifStructure(Request request) {
    	
		 Form<Structure> form= dataForm.form(Structure.class).bindFromRequest(request);
		 Structure pro = form.get();
		 	 String resultat=StructureService.editStructure(pro);
		 
	 if(resultat.equals("ok")) {
		 return redirect(routes.StructureCtroler.listeStructure())
				 .flashing("success", "Structure modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.StructureCtroler.formulaireStructure())
				 .flashing("error", "Structure non modifié");
	 }
	 
  }
	
	public Result editForm(String numStructure,Request request) {
    	
    	Structure p=StructureService.byNumStructure(numStructure);
    	
    	return ok(views.html.StructureModif.render(p,request));
    }
	
	public Result deleteStructure(String numStructure,Request request) {
    	
		 String resultat=StructureService.delStructure(numStructure);
		 if(resultat.equals("ok")) {
			 return redirect(routes.StructureCtroler.listeStructure())
					 .flashing("success", "Structure supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.StructureCtroler.formulaireStructure())
					 .flashing("error", "Structure non supprimé");
		 }
		 
	    }
	
	 public Result ProModif(Request request) {
	    	Form <Structure> form= dataForm.form(Structure.class).bindFromRequest(request);
	    	Structure misepro = form.get();
	    	if(StructureService.editStructure(misepro).equals("ok")) {
	    		
	    		System.out.print("Modification effectuée avec succes!");
	    		}
	    	else {
	    		System.out.print("Modification non effectuée");
	    	}
	    	return ok(views.html.Structureliste.render(StructureService.listStructure(), request));
	  }


}
