package controllers;

import java.util.List;

import Serivice.ProfilService;
import com.google.inject.Inject;
import models.tables.pojos.Profile;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.Request;

public class ProfilCtroler extends Controller{
	
	FormFactory dataForm;
	ProfilService pfilservice;
@Inject
	public ProfilCtroler(FormFactory dataForm,ProfilService pfilservice) {
		super();
		this.dataForm=dataForm;
		this.pfilservice=pfilservice;
	}
	
	public Result formulaireProfil(Request request) {
		return ok(views.html.ProfileView.render(request));
	}
	
	public Result listeprofil(Request request) {
		List<Profile> ps=pfilservice.getAll();
		return ok(views.html.Profileliste.render(ps, request));
	}
	

	 public  Result ajoutProfil(Request request) {
	    	
		 Form<Profile> form= dataForm.form(Profile.class).bindFromRequest(request);
		 Profile pro = form.get();
		 
		 //pro.setCreerPar("Login");
		
		 
		 if(pfilservice.isExistNumpro(pro.getNumProfile())){
			 return redirect(routes.ProfilCtroler.formulaireProfil())
					 .flashing("error", "Ce Profil "+pro.getNumProfile()+" existe déja");
		 }
		  String resultat=pfilservice.savePfil(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.ProfilCtroler.listeprofil())
				 .flashing("success", "Profil ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ProfilCtroler.formulaireProfil())
				 .flashing("error", "Profil non ajouté");
	 }
	 
  }
	 
	 public  Result modifProfril(Request request) {
	    	
		 Form<Profile> form= dataForm.form(Profile.class).bindFromRequest(request);
		 Profile pro = form.get();
		 
		 //pro.setCreerPar("Login");
		if(pfilservice.isLibelleExist(pro.getLibelleProfile())){
			 return redirect(routes.ProfilCtroler.formulaireProfil())
					 .flashing("error", "le libelle "+pro.getLibelleProfile()+" existe déja");
		 }
		 
		 String resultat=pfilservice.editPfil(pro);
		  if(resultat.equals("ok")) {
		 return redirect(routes.ProfilCtroler.listeprofil())
				 .flashing("success", "Profil modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ProfilCtroler.formulaireProfil())
				 .flashing("error", "Profil non modifié");
	 }
		 
	 }
	  
		 public Result editProfil(String numProfil,Request request) {
		    	
		    	Profile p=pfilservice.byNumprofil(numProfil);
		    	
		    	return ok(views.html.ProfileModif.render(p,request));
		    }

		 public Result deleteProfil(String numPro,Request request) {
		    	
			 String resultat=pfilservice.delProfil(numPro);
			 
			 
			 if(resultat.equals("ok")) {
				 return redirect(routes.ProfilCtroler.listeprofil())
						 .flashing("success", "Le profil supprimé avec success");
				 }
			 else {
				 System.out.println(resultat);
				 return redirect(routes.ProfilCtroler.formulaireProfil())
						 .flashing("error", "Profil non supprimé");
			 }
			 
		    }
}
