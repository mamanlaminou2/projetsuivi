package controllers;

import com.google.inject.Inject;

import Serivice.ActiviteService;
import Serivice.BailleurService;
import Serivice.FonctionService;
import Serivice.StructureService;
import Serivice.TypefService;
import Serivice.ActionService;
import models.tables.pojos.*;
import models.tables.pojos.Activite;

import play.data.FormFactory;
import play.libs.Files.TemporaryFile;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Result;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.data.Form;

public class ActiviteCtrole extends Controller {
	FormFactory dataForm;
	ActiviteService activiteService;
	ActionService actionService;
	StructureService structureService;
	BailleurService bailleurService;
	FonctionService fonctionService;
	TypefService typefService;
	@Inject
	public ActiviteCtrole(FormFactory dataForm, ActiviteService activiteService, ActionService actionService, StructureService structureService,
			BailleurService bailleurService,FonctionService fonctionService,TypefService typefService){
		this.dataForm =dataForm;
		this.activiteService =activiteService;
		this.actionService= actionService;
		this.structureService = structureService;
		this.bailleurService=bailleurService;
		this.fonctionService=fonctionService;
		this.typefService=typefService;
	}
	
	public Result ActiviteView(Request request){
		List<Action> ps=actionService.getAll();
		List<Bailleur> bl=bailleurService.getAll();
		List<Fonction> fc=fonctionService.getAll();
		List<Structure> sp=structureService.listStructure();
		List<Typefinancement> tf=typefService.getAll();
		return ok(views.html.ActiviteView.render(ps,bl,fc,sp,tf,request));
	}
	public Result formFile(Request request) {
		return ok(views.html.FileActivite.render(request));
	}
	
	public Result traitement(Request request) {
		 Http.MultipartFormData<TemporaryFile> body = request.body().asMultipartFormData();
		    Http.MultipartFormData.FilePart<TemporaryFile> picture = body.getFile("fichier");
		    List<Activite> lat=new ArrayList<>();
		    if (picture != null) {
		      String fileName = picture.getFilename();
		      long fileSize = picture.getFileSize();
		      String contentType = picture.getContentType();
		      TemporaryFile file = picture.getRef();
		      String extension=getExtension(fileName);
		      if(extension.equals("xlsx")){
		    	file.copyTo(Paths.get("C:/Users/user/workspace/projetsuivi/lib/activite.xlsx"), true);
		    	try {
		    		lat=load("C:/Users/user/workspace/projetsuivi/lib/activite.xlsx");
		    		
		    		boolean test=true;
		    		
		    		for (Activite activite : lat) {
						String resultat=activiteService.saveActi(activite);
						if(!resultat.equals("ok")){
							test=false;
							System.out.println(activite.getNumActivite());
						}
					}

		    		 if(test) {
		    			 return redirect(routes.ActiviteCtrole.Activiteliste())
		    					.flashing("success", "Activite ajouté avec success");
		    			 }
		    		 else {
		    			 //System.out.println(resultat);
		    			 return redirect(routes.ActiviteCtrole.ActiviteView())
		    					 .flashing("error", "Activite non ajouté");
		    		 }
		    		 
		    		 
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
		    	
			    }else{
		    	
		      }
		      
		  }else{
			 
		  }
		return ok(views.html.FileActivite.render(request));
	}
	
	private String getExtension(String fileName) {
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extension = fileName.substring(i + 1);
		}
		return extension;
	}
	
	 public List<Activite>  load(String path) {
	      List<Activite> activites=new ArrayList();

	      try {
	    	  FileInputStream fis = new FileInputStream(new File(path));
		      
		      XSSFWorkbook workbook = new XSSFWorkbook(fis);
		      XSSFSheet spreadsheet = workbook.getSheetAt(0);
	    	  int rows=spreadsheet.getLastRowNum();
				int cols=spreadsheet.getRow(1).getLastCellNum();
				
				
				
				for(int r=1;r<=rows;r++)
				{
					
					XSSFRow row=spreadsheet.getRow(r);
					Activite ac=new Activite();
					
					for(int c=0;c<cols;c++)
					{
						XSSFCell cell=row.getCell(c);
						
						if(c==0){
							System.out.println(cell.getStringCellValue()+" ---------0");
							ac.setNumActivite(cell.getStringCellValue());
					}else if (c==1) {
							System.out.println(cell.getStringCellValue()+" ---------1");
							ac.setLibelleActivite(cell.getStringCellValue());
					}else if (c==2) {
						System.out.println(cell.getStringCellValue()+" ---------2");
						ac.setIdAction(cell.getStringCellValue());}
					else if (c==3) {
						System.out.println(cell.getStringCellValue()+" --------3");		

						ac.setTypeActivite(cell.getStringCellValue());
					}
						else if (c==4) {
							System.out.println(cell.getStringCellValue()+" --------4");		

							ac.setLivrable(cell.getStringCellValue());
						}
						
						else if (c==5) {
							System.out.println(cell.getStringCellValue()+" --------5");		

							ac.setSourceVerification(cell.getStringCellValue());
						}
						
						else if (c==6) {
							System.out.println(cell.getStringCellValue()+" --------6");		

							ac.setStatut(cell.getStringCellValue());
						}
						
						else if (c==7) {
							System.out.println(cell.getStringCellValue()+" --------7");		

							ac.setIdBailleur(cell.getStringCellValue());
						}
						
						else if (c==8) {
							System.out.println(cell.getStringCellValue()+" --------8");		

							ac.setIdFonction(cell.getStringCellValue());
						}
						
						else if (c==9) {
							System.out.println(cell.getStringCellValue()+" --------9");		

							ac.setIdStructure(cell.getStringCellValue());
						}
						
						else if (c==10) {
							System.out.println(cell.getStringCellValue()+" --------10");		

							ac.setIdTypefinancement(cell.getStringCellValue());
						}
						
					else{
							//System.out.println(cell.getStringCellValue()+" --------11");		
							
							ac.setCreerPar("test");
							ac.setCreeLe(LocalDate.now());
							//p.setDelete(0);
						}
						
						//System.out.println(cell.getStringCellValue());
//			            switch (cell.getCellType()) {
//			               case Cell.CELL_TYPE_NUMERIC:
//			                  System.out.print(cell.getNumericCellValue() + " \t\t ");
//			                  break;
//			               
//			               case Cell.CELL_TYPE_STRING:
//			                  System.out.print(
//			                  cell.getStringCellValue() + " \t\t ");
//			                  break;
//			            }

					}
					
					activites.add(ac);
				}
				
					System.out.println(activites.size()+" +++++++++"); 
		      fis.close();
		} catch (Exception e) {
			System.out.println(e.getMessage()); 
		}
	      
	      
	    return activites;
	   
	 }
 
	

	public Result Activiteliste(Request request) {
		List<Activite> ps=activiteService.getAll();
		return ok(views.html.Activiteliste.render(ps, request));
	}
	
	
	public  Result ajoutActivite(Request request) {
    	
		 Form<Activite> form= dataForm.form(Activite.class).bindFromRequest(request);
		 Activite pro = form.get();
		 
		 pro.setCreerPar("Login");
		 //pro.setDelete(0);
		
		 
		 if(activiteService.isNumExist(pro.getNumActivite())){
			 return redirect(routes.ActiviteCtrole.ActiviteView())
					 .flashing("error", "Activite avec numro "+pro.getNumActivite()+" existe déja");
		 }
		 
		 
		 if(activiteService.isLibelleExist(pro.getLibelleActivite())){
			 return redirect(routes.ActiviteCtrole.ActiviteView())
					 .flashing("error", "Activite avec libelle "+pro.getLibelleActivite()+" existe déja");
		 }
		 
		 String resultat=activiteService.saveActi(pro);
	 if(resultat.equals("ok")) {
		 return redirect(routes.ActiviteCtrole.Activiteliste())
				 .flashing("success", "activite ajouté avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ActiviteCtrole.ActiviteView())
				 .flashing("error", "Activite non ajouté");
	 }
	 
  }
	
	public  Result ActiviteModif(Request request) {
    	
		 Form<Activite> form= dataForm.form(Activite.class).bindFromRequest(request);
		 Activite pro = form.get();
		 
		 pro.setCreerPar("Login");
		
		 
		 
		 
		 if(activiteService.isLibelleExist(pro.getLibelleActivite())){
			 return redirect(routes.ActiviteCtrole.ActiviteView())
					 .flashing("error", "Activite avec libelle "+pro.getLibelleActivite()+" existe déja");
		 }
		 
		 
		 
		 String resultat=activiteService.ediActi(pro);
		 
		 
	 if(resultat.equals("ok")) {
		 return redirect(routes.ActiviteCtrole.Activiteliste())
				 .flashing("success", "Activite modifié avec success");
		 }
	 else {
		 System.out.println(resultat);
		 return redirect(routes.ActiviteCtrole.ActiviteView())
				 .flashing("error", "Activite non modifié");
	 }
	 
  }
	
	public Result editActi(String numActivite,Request request) {
    	
    	Activite p=activiteService.getByNum(numActivite);
    	
    	return ok(views.html.ActiviteModif.render(p,request));
    }
	
	public Result delActi(String numActivite,Request request) {
    	
		 String resultat=activiteService.delActi(numActivite);
		 
		 
		 if(resultat.equals("ok")) {
			 return redirect(routes.ActiviteCtrole.Activiteliste())
					 .flashing("success", "Activite supprimé avec success");
			 }
		 else {
			 System.out.println(resultat);
			 return redirect(routes.ActiviteCtrole.ActiviteView())
					 .flashing("error", "Activite non supprimé");
		 }
		 
	    }
	
	 public Result ProModif(Request request) {
	    	Form <Activite> form= dataForm.form(Activite.class).bindFromRequest(request);
	    	Activite misepro = form.get();
	    	if(activiteService.ediActi(misepro).equals("ok")) {
	    		
	    		System.out.print("Modification effectuée avec succes!");
	    		}
	    	else {
	    		System.out.print("Modification non effectuée");
	    	}
	    	return ok(views.html.Activiteliste.render(activiteService.getAll(), request));
	  }

}
