package controllers;

import java.util.List;

import com.google.inject.Inject;

import Serivice.ActiviteService;
import Serivice.AffecterService;
import Serivice.AgentService;
import Serivice.StructureService;
import models.tables.pojos.Activite;
import models.tables.pojos.Affecter;
import models.tables.pojos.Agent;
import models.tables.pojos.Programme;
import models.tables.pojos.Structure;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.Request;

public class AffecterCtrole extends Controller {
	FormFactory dataForm;
	AffecterService affecterService;
	AgentService agentService;
	ActiviteService activiteService;
	StructureService structureService;
	@Inject
	public AffecterCtrole(FormFactory dataForm,AffecterService affecterService,AgentService agentService,ActiviteService activiteService,StructureService structureService){
		this.dataForm=dataForm;
		this.affecterService=affecterService;
		this.agentService=agentService;
		this.activiteService=activiteService;
		this.structureService=structureService;
	}
	public Result formulaireAffecter(Request request) {
		List<Agent> ag=agentService.getAll();
		List<Activite> ac=activiteService.getAll();
		List<Structure> sp=structureService.listStructure();

		return ok(views.html.AffecterView.render(ag,ac,sp,request));
	}
	
	public Result ListAffectation(Request request) {
		List<Affecter> ps=affecterService.getAll();
		return ok(views.html.Affecterliste.render(ps, request));
	}
	
	
	public  Result ajoutAffectation(Request request) {
    	
		 Form<Affecter> form= dataForm.form(Affecter.class).bindFromRequest(request);
		 Affecter affec = form.get();
		 String resultat=affecterService.saveAff(affec);
        if(resultat.equals("ok")){
	 return redirect(routes.ProgrammeCtrole.index())
		 .flashing("success", "Programme supprimé avec success");
		 }
        else {
			 System.out.println(resultat);
			 return redirect(routes.ProgrammeCtrole.formulaireProgramme())
					 .flashing("error", "Programme non supprimé");
		 }

		 
		 
		 
		 //pro.setCreerPar("Login");
		 //pro.setDelete(0);
		
		 
		 
	 
  }
	

}
