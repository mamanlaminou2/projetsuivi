package controllers;

import Serivice.POITratement;
import play.mvc.*;
import play.mvc.Http.Request;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {
	
	POITratement pt=new POITratement();

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index(Request request) {
    	try {
			//pt.XSSTraitement();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    	
        return ok(views.html.index.render(request));
    }

}
