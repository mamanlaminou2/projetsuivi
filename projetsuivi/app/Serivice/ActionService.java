package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.ActionDao;
import models.tables.pojos.Action;


public class ActionService extends ActionDao {
	 Connection  con;
	 public ActionService(){
		 this.con=JDBC.jdbc();
	 }
	 
		public  String saveAct(Action p) {
			 String req="insert into Action (id_Action, libelle_Action,num_programme ,cree_le, creer_par) values (?,?,?,?,?)";
	       
	       try {
	            PreparedStatement ps=con.prepareStatement(req);
	           ps.setString(1, p.getIdAction());
	           ps.setString(2, p.getLibelleAction());
	           ps.setDate(4, java.sql.Date.valueOf(java.time.LocalDate.now()));
	           ps.setString(5, p.getCreerPar());
	           ps.setString(3, p.getNumProgramme());
	           //ps.setByte(5, p.getDelete());
	             
	           ps.executeUpdate();
	           return "ok";
	           
	       } catch (Exception e) {
	    	   System.out.println("action err+++++++++++++++++++++++++++"+e.getMessage());
	      	 return "Erreur : "+e.getMessage();
	      	 
	       }	
	}
	
		public  String editAct(Action p) {
			 String req="update Action set libelle_Action=?,cree_le=?, creer_par=?, num_programme where id_Action=?";
	       
	       try {
	            PreparedStatement ps=con.prepareStatement(req);
	           ps.setString(5, p.getIdAction());
	           ps.setString(1, p.getLibelleAction());
	           ps.setDate(2, java.sql.Date.valueOf(java.time.LocalDate.now()));
	           ps.setString(3, p.getCreerPar());
	           ps.setString(4, p.getNumProgramme());
	   
	           
	           ps.executeUpdate();
	           return "ok";
	           
	       } catch (Exception e) {
	      	 return "Erreur : "+e.getMessage();
	      	 
	       }	
	}
		
		public String delAct(String idAction) {

			 String req="update action set delete = 1 where num_programme=?";
		        
		        try {
		             PreparedStatement ps=con.prepareStatement(req);
		            ps.setString(1, idAction);
		            
		            ps.executeUpdate();
		            return "ok";
		            
		        } catch (Exception e) {
		       	 return "Erreur : "+e.getMessage();
		       	 
		        }	
		}
		
		
		public  List<Action> getAll() {
			
			List<Action> action = new ArrayList();
			
			String req="select * from action where delete=0";
			
			try {
				 PreparedStatement ps=con.prepareStatement(req);
				 ps.executeQuery();
				ResultSet rslt= ps.getResultSet();

				while (rslt.next()) {
					Action p=new Action();
					p.setIdAction(rslt.getString("id_action"));
					p.setLibelleAction(rslt.getString("libelle_action"));
					p.setNumProgramme(rslt.getString("num_programme"));
					action.add(p);
				}
			} catch (Exception e) {
				return null;
			}

			System.out.println(action.size());
			return action;
			
		}
		
		public Action getById(String idAction) {

			Action p = new Action();
			String req="select * from action where id_action =? and delete=0";
			
			try {
				 PreparedStatement ps=con.prepareStatement(req);
		         ps.setString(1, idAction);
				ResultSet rslt;
				rslt = ps.executeQuery();

				if (rslt.next()) {
					p.setIdAction(rslt.getString("id_action"));
					p.setLibelleAction(rslt.getString("libelle_action"));
				}
			} catch (Exception e) {
				return null;
			}

			return p;
			
			}
		
		public boolean isIdExist(String idAction) {

			boolean test=false;
			int i=0;
			
			try {
				String req="select * from action where id_action =? and delete=0";
				 PreparedStatement ps=con.prepareStatement(req);
		         ps.setString(1, idAction);
				ResultSet rslt;
				rslt = ps.executeQuery();
				
				if (rslt.next()) {
					++i;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			
			if(i > 0)
				test=true;
			
			
			return test;
			
			}
		
		public boolean isLibelleExist(String libelle) {

			boolean test=false;
			int i=0;
			
			
			try {
				String req="select * from action where libelle_action =? and delete=0 ";
				 PreparedStatement ps=con.prepareStatement(req);
		         ps.setString(1, libelle);
				ResultSet rslt;
				rslt = ps.executeQuery();
				
				if (rslt.next()) {
					++i;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			
			if(i > 0)
				test=true;
			
			
			return test;
			
			}
		
}
