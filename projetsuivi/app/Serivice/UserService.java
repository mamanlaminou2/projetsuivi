package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.UserDao;
import models.tables.pojos.Programme;
import models.tables.pojos.User;

public class UserService extends UserDao {
	
	Connection  con;
	public  UserService() {
		this.con=JDBC.jdbc();
	}
	
	public  String saveUser(User u) {
		 String req="insert into user (id_user, nom,prenom,pass,id_profile) values (?,?,?,?,?)";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setLong(1, u.getIdUser());
          ps.setString(2, u.getNom());
          ps.setString(3,u.getPernom());
          ps.setString(4, u.getPass());
          ps.setString(5, u.getIdProfile());
            
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	public  String editUser(User u) {
		 String req="update user set nom=?,prenom=?,pass=?,id_profile=? where id_user=?";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setLong(5, u.getIdUser());
          ps.setString(1, u.getNom());
          ps.setString(2, u.getPernom());
          ps.setString(3, u.getPass());
          ps.setString(4, u.getIdProfile());
  
          
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
	public String delUser(String idUser) {

		 String req="update user set delete = 1 where id_user=?";
	        
	        try {
	             PreparedStatement ps=con.prepareStatement(req);
	            ps.setString(1, idUser);
	            
	            ps.executeUpdate();
	            return "ok";
	            
	        } catch (Exception e) {
	       	 return "Erreur : "+e.getMessage();
	       	 
	        }	
	}
	
public  List<User> getAll() {
		
		List<User> users = new ArrayList();
		
		String req="select * from user where delete=0";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				User p=new User();
				p.setIdUser(rslt.getLong("id_user"));
				p.setNom(rslt.getString("nom"));
				p.setPernom(rslt.getString("prenom"));
				p.setIdProfile(rslt.getString("id_prolfile"));
				users.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(users.size());
		return users;
		
	}

}
