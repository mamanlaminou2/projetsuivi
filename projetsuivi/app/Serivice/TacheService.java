package Serivice;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.pojos.Activite;
import models.tables.pojos.Tache;

public class TacheService {
	
	Connection con;

	public TacheService() {
		super();
		this.con=JDBC.jdbc();
	}


	public String saveTache(Tache r) {
		String req= "insert into tache (id_tache,libelle_tache,num_activite,cree_le,creer_par,id_nature) values (?,?,?,?,?,?)";
	try {
		PreparedStatement ps= con.prepareStatement(req);
		
		ps.setString(1, r.getIdTache());
		ps.setString(2, r.getLibelleTache());
		ps.setString(3, r.getNumActivite());
		ps.setDate(4, java.sql.Date.valueOf(java.time.LocalDate.now()));
		ps.setString(5,r.getCreerPar());
		ps.setString(6, r.getIdNature());
		ps.executeUpdate();
		return"ok";
		
	} catch (Exception e) {
		return "erreur"+ e.getMessage(); 
	}
	}


	public String editTache(Tache r) {
		String req="update tache set libelle_tache=?,num_activite=?,cree_le=?,creer_par=?,id_nature=? where id_ache=?";
		try {
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1, r.getLibelleTache());
			ps.setString(2,   r.getNumActivite());
			ps.setDate(3,java.sql.Date.valueOf(java.time.LocalDate.now()));
			ps.setString(4, r.getCreerPar());
			ps.setString(5, r.getIdNature());
			ps.setString(6, r.getIdTache());
			ps.executeUpdate();
			return"ok";
			
		} catch (Exception e) {
			return "erreur"+ e.getMessage(); 	
		}
	}

	public List<Tache> listTache(){
		List<Tache> Taches = new ArrayList();
		Tache pro =new Tache();
		String req ="select * from tache where delete=0";
		try {
			PreparedStatement ps= con.prepareStatement(req);
			 ps.executeQuery();
				ResultSet rslt= ps.getResultSet();
		while(rslt.next()) {
			pro.setIdTache(rslt.getString("id_tache"));
			pro.setLibelleTache(rslt.getString("libelle_tache"));
			pro.setNumActivite(rslt.getString("num_activite"));
			pro.setCreerPar(rslt.getString("creer_par"));
			pro.setIdNature(rslt.getString("id_nature"));
			
			Taches.add(pro);
			
		}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			//return null;
		}
		return Taches;

	}
	public String delTache (String delrapp) {
		String req="update Tache set delete=1 where id_tache=?";
		try {
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1, delrapp);
			ps.executeUpdate();
			return "ok";
		} catch (Exception e) {
			return "Erreur"+e.getMessage();
		}
	}

	public Tache byNumTache(String numrapp){
		String req="select from tache where id_tache=? and delete=1;";
		try {
			Tache rap =new Tache();
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1,numrapp);
			
			ResultSet rslt = ps.executeQuery();
			if(rslt.next()) {
			
				rap.setIdTache(rslt.getString("id_tache"));
				rap.setNumActivite(rslt.getString("num_activite"));
				rap.setCreerPar(rslt.getString("creer_le"));
			}
			return rap;
			
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isExistNumRapprot(String numrapp){
		boolean exit =false;
		int i=0;
		String req="select * from tache where id_tache=? and delete=0";
		//String req="select * from Profil where num_profil=? or libelle_profil=?";
		try {
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1,numrapp);
			//ps.setString(2,libellePro);
			ResultSet rslt = ps.executeQuery();
			if(rslt.next()) {
				++i;
			}
			
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		if(i>0) {
			exit=true;
		}
		return exit;
	}


public  List<Activite> listActivite() {
		
		List<Activite> activite = new ArrayList();
		
		String req="select * from activite";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Activite p=new Activite();
				//p.setNumActivite(rslt.getString("num_activite"));
				p.setLibelleActivite(rslt.getString("libelle_activite"));
				//p.setIdAction(rslt.getString("id_action"));
				
				activite.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(activite.size());
		return activite;
		
	}

}
