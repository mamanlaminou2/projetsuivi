package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.AffecterDao;
import models.tables.pojos.Affecter;

public class AffecterService extends AffecterDao{
	Connection  con;
	public  AffecterService() {
		this.con=JDBC.jdbc();
	}

	public  String saveAff(Affecter a) {
		 String req="insert into affecter (date_affection,mat_agent,num_activite,structure) values (?,?,?,?)";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now()));
          ps.setString(2, a.getMatAgent());
          ps.setString(3, a.getNumActivite());
          ps.setString(4, a.getStructure());
            
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
	public  String editAff(Affecter a) {
		 String req="update affecter set date_affection=?,structure=? where mat_agent=? and num_activite=? ";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now()));
          ps.setString(3, a.getMatAgent());
          ps.setString(4, a.getNumActivite());
          ps.setString(2, a.getStructure());
  
          
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
public  List<Affecter> getAll() {
		
		List<Affecter> affecte = new ArrayList();
		
		String req="select * from affecter";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Affecter a=new Affecter();
				a.setMatAgent(rslt.getString("mat_agent"));
				a.setNumActivite(rslt.getString("num_activite"));
				a.setStructure(rslt.getString("structure"));
				
				affecte.add(a);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(affecte.size());
		return affecte;
		
	}
	
	
	
	
}
