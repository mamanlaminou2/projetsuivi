package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.pojos.Profile;

public class ProfilService {
Connection con;

public ProfilService() {
	super();
	this.con=JDBC.jdbc();
}

public String savePfil(Profile p) {
	String req= "insert into Profile (num_profile,libelle_profile) values (?,?)";
try {
	PreparedStatement ps= con.prepareStatement(req);
	ps.setString(1, p.getNumProfile());
	ps.setString(2, p.getLibelleProfile());
	//ps.setDate(3, java.sql.Date.valueOf(java.time.LocalDate.now()));
	
	ps.executeUpdate();
	return"ok";
	
} catch (Exception e) {
	return "erreur"+ e.getMessage(); 
}
}

public String editPfil(Profile p) {
	String req="update Profile set libelle_profile=? where num_profile=?";
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1, p.getLibelleProfile());
		ps.executeUpdate();
		return"ok";
		
	} catch (Exception e) {
		return "erreur"+ e.getMessage(); 	
	}
}

public List<Profile> getAll(){
	List<Profile> profils =new ArrayList();
	
	String req ="select * from Profile where delete=0";
	try {
		PreparedStatement ps= con.prepareStatement(req);
		 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();
	while(rslt.next()) {
		Profile pro =new Profile();
		pro.setNumProfile(rslt.getString("num_profile"));
		pro.setLibelleProfile(rslt.getString("libelle_profile"));
		pro.setDelete(rslt.getByte("delete"));
		profils.add(pro);
		
	}
	} catch (Exception e) {
		return null;
	}
	return profils;
}

public String delProfil (String delpro) {
	String req="update profile set delete=1 where num_profile=?";
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1, delpro);
		ps.executeUpdate();
		return "ok";
	} catch (Exception e) {
		return "Erreur"+e.getMessage();
	}
}

public Profile byNumprofil(String Pro) {
	String req="select from Profile where num_profile=? and delete=0";
	Profile profils =new Profile();
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1,Pro);
		ResultSet rslt = ps.executeQuery();
		if(rslt.next()) {
			profils.setLibelleProfile(rslt.getString("libelle_profile"));
			
			}
		}
		 catch (Exception e) {
		return null;
	}
	return profils;
}
	
//public boolean isExistNumpro(String numPro,String libellepro){
public boolean isExistNumpro(String numPro){
	boolean exit =false;
	int i=0;
	String req="select * from Profile where num_profil=?";
	//String req="select * from Profil where num_profil=? or libelle_profil=?";
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1,numPro);
		//ps.setString(2,libellePro);
		ResultSet rslt = ps.executeQuery();
		if(rslt.next()) {
			++i;
		}
		
			
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	if(i>0) {
		exit=true;
	}
	return exit;
}

public boolean isLibelleExist(String libelle) {

	boolean test=false;
	int i=0;
	
	
	try {
		String req="select * from profile where libelle_profile =? and delete=0 ";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, libelle);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}

}
