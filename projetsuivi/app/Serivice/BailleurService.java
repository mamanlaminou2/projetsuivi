package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.BailleurDao;
import models.tables.pojos.Bailleur;

public class BailleurService extends BailleurDao {
	
	Connection  con;
	public  BailleurService() {
		this.con=JDBC.jdbc();
	}
	
	public  String saveBail(Bailleur b) {
		 String req="insert into bailleur (id_bailleur, libelle_bailleur) values (?,?)";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setString(1, b.getIdBailleur());
          ps.setString(2, b.getLibelleBailleur());
          
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
	public  String editBail(Bailleur b) {
		 String req="update bailleur set libelle_bailleur=? where id_bailleur=?";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
           ps.setString(2, b.getIdBailleur());
           ps.setString(1, b.getLibelleBailleur());
          
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
public  List<Bailleur> getAll() {
		
		List<Bailleur> bailleurs = new ArrayList();
		
		String req="select * from bailleur";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Bailleur p=new Bailleur();
				p.setIdBailleur(rslt.getString("id_bailleur"));
				p.setLibelleBailleur(rslt.getString("libelle_bailleur"));
				bailleurs.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(bailleurs.size());
		return bailleurs;
		
	}
	
}
