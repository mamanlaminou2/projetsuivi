package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;

import com.google.inject.Inject;

import models.Tables;
import models.tables.daos.AgentDao;
import models.tables.pojos.Agent;

public class AgentService extends AgentDao {
	Connection  con;
	public  AgentService() {
		this.con=JDBC.jdbc();
	}
	
	public  String savePro(Agent p) {
		 String req="insert into agent (mat_agent,nom,prenom,telephone,datenaissance,lieunaissance,num_profile,id_structure)"
		 		+ " values (?,?,?,?,?,?,?,?)";
       
       try {
            PreparedStatement ps=con.prepareStatement(req);
            ps.setString(1, p.getMatAgent());
            ps.setString(2, p.getNom());
            ps.setString(3, p.getPrenom());
            ps.setLong(4,   p.getTelephone());
            ps.setString(5, p.getDatenaissance());
            ps.setString(6, p.getLieunaissance());
            ps.setString(7, p.getNumProfile());
            ps.setString(8, p.getIdStructure()); 
            
           ps.executeUpdate();
           return "ok";
           
       } catch (Exception e) {
      	 return "Erreur : "+e.getMessage();
      	 
       }	
}
	
	public  String editAgent(Agent p) {
		 String req="update agent set nom=?,prenom=?,telephone=?,datenaissance=?,lieunaissance=?,"
		 		+ "num_profile=?,id_structure=? where mat_agent=?";
       
       try {
            PreparedStatement ps=con.prepareStatement(req);
           ps.setString(1, p.getNom());
           ps.setString(2, p.getPrenom());
           ps.setLong(3, p.getTelephone());
           ps.setString(4, p.getDatenaissance());
           ps.setString(5, p.getLieunaissance());
           ps.setString(6, p.getNumProfile());
           ps.setString(7, p.getIdStructure());
           ps.setString(8, p.getMatAgent());
   
           
           ps.executeUpdate();
           return "ok";
           
       } catch (Exception e) {
      	 return "Erreur : "+e.getMessage();
      	 
       }	
}
	public String delAgent(String matAgent) {

		 String req="update agent set delete = 1 where mat_agent=?";
	        
	        try {
	             PreparedStatement ps=con.prepareStatement(req);
	            ps.setString(1, matAgent);
	            
	            ps.executeUpdate();
	            return "ok";
	            
	        } catch (Exception e) {
	       	 return "Erreur : "+e.getMessage();
	       	 
	        }	
	}
	
public  List<Agent> getAll() {
		
		List<Agent> agents = new ArrayList();
		
		String req="select * from agent where delete=0";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Agent p=new Agent();
				p.setMatAgent(rslt.getString("mat_agent"));
				p.setNom(rslt.getString("nom"));
				p.setPrenom(rslt.getString("prenom"));
				p.setTelephone(rslt.getLong("telephone"));
				p.setDatenaissance(rslt.getString("datenaissance"));
				p.setLieunaissance(rslt.getString("lieunaissance"));
				p.setNumProfile(rslt.getString("num_profile"));
				p.setLieunaissance(rslt.getString("id_structure"));
				agents.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(agents.size());
		return agents;
		
	}


public Agent getByNum(String matAgent) {

	Agent p = new Agent();
	String req="select * from agent where mat_agent =? and delete=0";
	
	try {
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, matAgent);
		ResultSet rslt;
		rslt = ps.executeQuery();

		if (rslt.next()) {
			p.setMatAgent(rslt.getString("mat_agent"));
			p.setNom(rslt.getString("nom"));
			p.setPrenom(rslt.getString("prenom"));
			p.setTelephone(rslt.getLong("telephone"));
			p.setDatenaissance(rslt.getString("datenaissance"));
			p.setLieunaissance(rslt.getString("lieunaissance"));
			p.setNumProfile(rslt.getString("num_profile"));
			p.setLieunaissance(rslt.getString("id_structure"));

			

		}
	} catch (Exception e) {
		return null;
	}

	return p;
	
	}

public boolean isNumExist(String matAgent) {

	boolean test=false;
	int i=0;
	
	try {
		String req="select * from agent where mat_agent =? and delete=0";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, matAgent);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}

public boolean isLibelleExist(String nom) {

	boolean test=false;
	int i=0;
	
	
	try {
		String req="select * from agent where nom =? and delete=0 ";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, nom);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}
	

}

