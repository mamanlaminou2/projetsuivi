package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.ActiviteDao;
import models.tables.pojos.Activite;
//import models.tables.pojos.Programme;

public class ActiviteService extends ActiviteDao {
	Connection con;
	public ActiviteService(){
		this.con=JDBC.jdbc();
		}
	public  String saveActi(Activite p) {
		 String req="insert into activite "
		 		+ "(num_activite, libelle_activite,id_action,cree_le, creer_par,id_structure,type_activite,id_bailleur,id_fonction,id_typefinancement,livrable,source_verification,statut)"
		 		+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setString(1,p.getNumActivite());
          ps.setString(2,p.getLibelleActivite());
          ps.setString(3,p.getIdAction());
          ps.setDate(4, java.sql.Date.valueOf(java.time.LocalDate.now()));
          ps.setString(5,p.getCreerPar());
          ps.setString(6,p.getIdStructure());
          ps.setString(7,p.getTypeActivite());
          ps.setString(8,p.getIdBailleur());
          ps.setString(9,p.getIdFonction());
          ps.setString(10,p.getIdTypefinancement());
          ps.setString(11,p.getLivrable());
          ps.setString(12,p.getSourceVerification());
          ps.setString(13,p.getStatut());
            
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
    	  System.out.println("Erreur activite---------------"+e.getMessage());
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
	public  String ediActi(Activite p) {
		 String req="update activite set "
		 		+ "libelle_activite=?,id_action=?,cree_le=? ,creer_par=?,id_structure=?,type_activite=?,id_bailleur=?,id_fonction=?,id_typefinancement=?,livrable=?,source_verification=?,statut=? where num_activite=?";
      
      try {
           PreparedStatement ps=con.prepareStatement(req);
          ps.setString(13, p.getNumActivite());
          ps.setString(1, p.getLibelleActivite());
          ps.setString(2, p.getIdAction());
          ps.setDate(3, java.sql.Date.valueOf(java.time.LocalDate.now()));
          ps.setString(4, p.getCreerPar());
          ps.setString(5, p.getIdStructure());
          ps.setString(6, p.getTypeActivite());
          ps.setString(7,p.getIdBailleur());
          ps.setString(8,p.getIdFonction());
          ps.setString(9, p.getIdTypefinancement());
          ps.setString(10, p.getLivrable());
          ps.setString(11, p.getSourceVerification());
          ps.setString(12, p.getStatut());
  
          
          ps.executeUpdate();
          return "ok";
          
      } catch (Exception e) {
     	 return "Erreur : "+e.getMessage();
     	 
      }	
}
	
	public String delActi(String IdActivite) {

		 String req="update activite set delete = 1 where id_activite=?";
	        
	        try {
	             PreparedStatement ps=con.prepareStatement(req);
	            ps.setString(1, IdActivite);
	            
	            ps.executeUpdate();
	            return "ok";
	            
	        } catch (Exception e) {
	       	 return "Erreur : "+e.getMessage();
	       	 
	        }	
	}
	
public  List<Activite> getAll() {
		
		List<Activite> activite = new ArrayList();
		
		String req="select * from activite where delete=0";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Activite p=new Activite();
				p.setNumActivite(rslt.getString("num_activite"));
				p.setLibelleActivite(rslt.getString("libelle_activite"));
				p.setIdAction(rslt.getString("id_action"));
				p.setIdStructure(rslt.getString("id_structure"));
				p.setTypeActivite(rslt.getString("type_activite"));
				p.setIdBailleur(rslt.getString("id_bailleur"));
		        p.setIdFonction(rslt.getString("id_fonction"));
		        p.setIdTypefinancement(rslt.getString("id_typefinancement"));
		        p.setLivrable(rslt.getString("livrable"));
		        p.setSourceVerification(rslt.getString("source_verification"));
		        p.setStatut(rslt.getString("statut"));
				
				activite.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(activite.size());
		return activite;
		
	}

public Activite getByNum(String numActi) {

	Activite p = new Activite();
	String req="select * from activite where num_activite =? and delete=0";
	
	try {
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, numActi);
		ResultSet rslt;
		rslt = ps.executeQuery();

		if (rslt.next()) {
			p.setNumActivite(rslt.getString("num_activite"));
			p.setLibelleActivite(rslt.getString("libelle_activite"));
		}
	} catch (Exception e) {
		return null;
	}

	return p;
	
	}

public boolean isNumExist(String numActi) {

	boolean test=false;
	int i=0;
	
	try {
		String req="select * from activite where num_activite =? and delete=0";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, numActi);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}

public boolean isLibelleExist(String libelle) {

	boolean test=false;
	int i=0;
	
	
	try {
		String req="select * from activite where libelle_activite =? and delete=0 ";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, libelle);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}


}
