package Serivice;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import models.tables.pojos.Rapport;

public class RapportService {
Connection con;

public RapportService() {
	super();
	this.con=JDBC.jdbc();
}


public String saveRapport(Rapport r) {
	String req= "insert into Rapport (numRapport,contenu,date,mat_agent) values (?,?,?,?)";
try {
	PreparedStatement ps= con.prepareStatement(req);
	
	ps.setString(1, r.getNumRapport());
	ps.setString(2, r.getContenu());
	ps.setString(4, r.getMatAgent());
	ps.setString(3, r.getDate());
	
	
	ps.executeUpdate();
	return"ok";
	
} catch (Exception e) {
	return "erreur"+ e.getMessage(); 
}
}


public String editRapport(Rapport r) {
	String req="update Rapport set contenu=?,mat_agent=? where num_rapport=?";
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1, r.getContenu());
		ps.setString(2, r.getMatAgent());
		
		
		
		ps.executeUpdate();
		return"ok";
		
	} catch (Exception e) {
		return "erreur"+ e.getMessage(); 	
	}
}

public List<Rapport> listRapport(){
	List<Rapport> Rapports = new ArrayList();
	Rapport pro =new Rapport();
	String req ="select * from Rapport where delete=0";
	try {
		PreparedStatement ps= con.prepareStatement(req);
		 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();
	while(rslt.next()) {
		pro.setNumRapport(rslt.getString("num_rapport"));
		pro.setContenu(rslt.getString("contenu"));
		pro.setDate(String.valueOf(rslt.getString("date")));
		pro.setMatAgent(rslt.getString("mat_agent"));
		
		Rapports.add(pro);
		
	}
	} catch (Exception e) {
		return null;
	}
	return Rapports;

}
public String delRapport (String delrapp) {
	String req="update Rapport set delete=1 where num_rapport=?";
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1, delrapp);
		ps.executeUpdate();
		return "ok";
	} catch (Exception e) {
		return "Erreur"+e.getMessage();
	}
}

public Rapport byNumrapport(String numrapp){
	String req="select from Rapport where num_rapport=? and delet=1;";
	try {
		Rapport rap =new Rapport();
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1,numrapp);
		
		ResultSet rslt = ps.executeQuery();
		if(rslt.next()) {
		rap.setNumRapport(rslt.getString("num_rapport"));
			rap.setContenu(rslt.getString("contenu"));
			rap.setDate(String.valueOf(rslt.getDate("date")));
			rap.setMatAgent(rslt.getString("mat_agent"));
			
			}
		return rap;
		
	} catch (Exception e) {
		return null;
	}
}

public boolean isExistNumRapprot(String numrapp){
	boolean exit =false;
	int i=0;
	String req="select * from Rapport where num_rapport=? and delete=0";
	//String req="select * from Profil where num_profil=? or libelle_profil=?";
	try {
		PreparedStatement ps=con.prepareStatement(req);
		ps.setString(1,numrapp);
		//ps.setString(2,libellePro);
		ResultSet rslt = ps.executeQuery();
		if(rslt.next()) {
			++i;
		}
		
			
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	if(i>0) {
		exit=true;
	}
	return exit;
}

}






