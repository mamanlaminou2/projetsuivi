package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.FonctionDao;
import models.tables.pojos.Fonction;

public class FonctionService extends FonctionDao {
	
	Connection  con;
	public  FonctionService() {
		this.con=JDBC.jdbc();
	}
	
	public  String saveFonc(Fonction f) {
		 String req="insert into fonction (id_fonction, libelle_fonction) values (?,?)";
     
     try {
          PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, f.getIdFonction());
         ps.setString(2, f.getLibelleFonction());
         
         ps.executeUpdate();
         return "ok";
         
     } catch (Exception e) {
    	 return "Erreur : "+e.getMessage();
    	 
     }	
}
	
	public  String editFonc(Fonction f) {
		 String req="update fonction set libelle_fonction=? where id_fonction=?";
     
     try {
          PreparedStatement ps=con.prepareStatement(req);
          ps.setString(2, f.getIdFonction());
          ps.setString(1, f.getLibelleFonction());
         
         ps.executeUpdate();
         return "ok";
         
     } catch (Exception e) {
    	 return "Erreur : "+e.getMessage();
    	 
     }	
}
	
public  List<Fonction> getAll() {
		
		List<Fonction> fonctions = new ArrayList();
		
		String req="select * from fonction";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Fonction p=new Fonction();
				p.setIdFonction(rslt.getString("id_fonction"));
				p.setLibelleFonction(rslt.getString("libelle_fonction"));
				fonctions.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(fonctions.size());
		return fonctions;
		
	}

}
