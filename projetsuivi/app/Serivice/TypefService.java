package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.TypefinancementDao;
import models.tables.pojos.Typefinancement;;

public class TypefService extends TypefinancementDao{
	
	Connection  con;
	public  TypefService() {
		this.con=JDBC.jdbc();
	}
	
	public  String saveTyp(Typefinancement t) {
		 String req="insert into typefinancement (id_typefinancement, libelle_typefinancement) values (?,?)";
    
    try {
         PreparedStatement ps=con.prepareStatement(req);
        ps.setString(1, t.getIdTypefinancement());
        ps.setString(2, t.getLibelleTypefinancement());
        
        ps.executeUpdate();
        return "ok";
        
    } catch (Exception e) {
   	 return "Erreur : "+e.getMessage();
   	 
    }	
}
	
	public  String editTyp(Typefinancement t) {
		 String req="update typefinancement set libelle_typefinancement=? where id_typefinancement=?";
    
    try {
         PreparedStatement ps=con.prepareStatement(req);
         ps.setString(2, t.getIdTypefinancement());
         ps.setString(1, t.getLibelleTypefinancement());
        
        ps.executeUpdate();
        return "ok";
        
    } catch (Exception e) {
   	 return "Erreur : "+e.getMessage();
   	 
    }	
}
	
public  List<Typefinancement> getAll() {
		
		List<Typefinancement> typefinancements = new ArrayList();
		
		String req="select * from typefinancement";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Typefinancement p=new Typefinancement();
				p.setIdTypefinancement(rslt.getString("id_typefinancement"));
				p.setLibelleTypefinancement(rslt.getString("libelle_typefinancement"));
				typefinancements.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(typefinancements.size());
		return typefinancements;
		
	}

}
