package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;

import com.google.inject.Inject;

import models.Tables;
import models.tables.daos.ProgrammeDao;
import models.tables.pojos.Programme;

public class ProgrammeService extends ProgrammeDao {
	Connection  con;
	public  ProgrammeService() {
		this.con=JDBC.jdbc();
	}
	
	public  String savePro(Programme p) {
		 String req="insert into programme (num_programme, libelle_programme,cree_le, creer_par,annee) values (?,?,?,?,?)";
       
       try {
            PreparedStatement ps=con.prepareStatement(req);
           ps.setString(1, p.getNumProgramme());
           ps.setString(2, p.getLibelleProgramme());
           ps.setDate(3, java.sql.Date.valueOf(java.time.LocalDate.now()));
           ps.setString(4, p.getCreerPar());
           ps.setString(5, p.getAnnee());
           //ps.setByte(5, p.getDelete());
             
           ps.executeUpdate();
           return "ok";
           
       } catch (Exception e) {
      	 return "Erreur : "+e.getMessage();
      	 
       }	
}
	
	public  String editPro(Programme p) {
		 String req="update programme set libelle_programme=?,cree_le=?, creer_par=?,annee=? where num_programme=?";
       
       try {
            PreparedStatement ps=con.prepareStatement(req);
           ps.setString(5, p.getNumProgramme());
           ps.setString(1, p.getLibelleProgramme());
           ps.setDate(2, java.sql.Date.valueOf(java.time.LocalDate.now()));
           ps.setString(4, p.getAnnee());
           ps.setString(3, p.getCreerPar());
   
           
           ps.executeUpdate();
           return "ok";
           
       } catch (Exception e) {
      	 return "Erreur : "+e.getMessage();
      	 
       }	
}
	public String delPro(String numProgramme) {

		 String req="update programme set delete = 1 where num_programme=?";
	        
	        try {
	             PreparedStatement ps=con.prepareStatement(req);
	            ps.setString(1, numProgramme);
	            
	            ps.executeUpdate();
	            return "ok";
	            
	        } catch (Exception e) {
	       	 return "Erreur : "+e.getMessage();
	       	 
	        }	
	}
	
public  List<Programme> getAll() {
		
		List<Programme> programmes = new ArrayList();
		
		String req="select * from programme where delete=0";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Programme p=new Programme();
				p.setNumProgramme(rslt.getString("num_programme"));
				p.setLibelleProgramme(rslt.getString("libelle_programme"));
				programmes.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(programmes.size());
		return programmes;
		
	}


public Programme getByNum(String numPro) {

	Programme p = new Programme();
	String req="select * from programme where num_programme =? and delete=0";
	
	try {
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, numPro);
		ResultSet rslt;
		rslt = ps.executeQuery();

		if (rslt.next()) {
			p.setNumProgramme(rslt.getString("num_programme"));
			p.setLibelleProgramme(rslt.getString("libelle_programme"));
		}
	} catch (Exception e) {
		return null;
	}

	return p;
	
	}

public boolean isNumExist(String numPro) {

	boolean test=false;
	int i=0;
	
	try {
		String req="select * from programme where num_programme =? and delete=0";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, numPro);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}

public boolean isLibelleExist(String libelle) {

	boolean test=false;
	int i=0;
	
	
	try {
		String req="select * from programme where libelle_programme =? and delete=0 ";
		 PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, libelle);
		ResultSet rslt;
		rslt = ps.executeQuery();
		
		if (rslt.next()) {
			++i;
		}

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
	
	if(i > 0)
		test=true;
	
	
	return test;
	
	}
	

}
