package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.daos.NatureeconomiqueDao;
import models.tables.pojos.Natureeconomique;

public class NatureService extends NatureeconomiqueDao {
	
	Connection  con;
	public  NatureService() {
		this.con=JDBC.jdbc();
	}
	
	public  String saveNat(Natureeconomique n) {
		 String req="insert into natureeconomique (id_nature, libelle_nature) values (?,?)";
     
     try {
          PreparedStatement ps=con.prepareStatement(req);
         ps.setString(1, n.getIdNature());
         ps.setString(2, n.getLibelleNature());
         
         ps.executeUpdate();
         return "ok";
         
     } catch (Exception e) {
    	 return "Erreur : "+e.getMessage();
    	 
     }	
}
	
	public  String editNat(Natureeconomique n) {
		 String req="update natureeconomique set libelle_nature=? where id_nature=?";
     
     try {
          PreparedStatement ps=con.prepareStatement(req);
          ps.setString(1, n.getIdNature());
          ps.setString(2, n.getLibelleNature());
         
         ps.executeUpdate();
         return "ok";
         
     } catch (Exception e) {
    	 return "Erreur : "+e.getMessage();
    	 
     }	
}
	
public  List<Natureeconomique> getAll() {
		
		List<Natureeconomique> natureeconomiques = new ArrayList();
		
		String req="select * from natureeconomique";
		
		try {
			 PreparedStatement ps=con.prepareStatement(req);
			 ps.executeQuery();
			ResultSet rslt= ps.getResultSet();

			while (rslt.next()) {
				Natureeconomique p=new Natureeconomique();
				p.setIdNature(rslt.getString("id_nature"));
				p.setLibelleNature(rslt.getString("libelle_nature"));
				natureeconomiques.add(p);
			}
		} catch (Exception e) {
			return null;
		}

		System.out.println(natureeconomiques.size());
		return natureeconomiques;
		
	}

}
