package Serivice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.tables.pojos.Structure;

public class StructureService {
	Connection con;

	public StructureService() {
		super();
		this.con=JDBC.jdbc();
	}


	public String saveStructure(Structure r) {
		String req= "insert into Structure(id_structure,libelle_tructure) values (?,?)";
	try {
		PreparedStatement ps= con.prepareStatement(req);
		
		ps.setString(1, r.getIdStructure());
		ps.setString(2, r.getLibelleStructure());
		ps.executeUpdate();
		return"ok";
		
	} catch (Exception e) {
		return "erreur"+ e.getMessage(); 
	}
	}


	public String editStructure(Structure r) {
		String req="update Structure set libelle_structure=? where id_Structure=?";
		try {
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1, r.getLibelleStructure());
			ps.executeUpdate();
			return"ok";
			
		} catch (Exception e) {
			return "erreur"+ e.getMessage(); 	
		}
	}

	public List<Structure> listStructure(){
		List<Structure> Structures = new ArrayList();
		
		String req ="select * from Structure where delete=0";
		try {
			PreparedStatement ps= con.prepareStatement(req);
			 ps.executeQuery();
				ResultSet rslt= ps.getResultSet();
		while(rslt.next()) {
			Structure pro =new Structure();
			pro.setIdStructure(rslt.getString("id_Structure"));
			pro.setLibelleStructure(rslt.getString("libelle_structure"));
			System.out.println(rslt.getString("libelle_structure"));
			Structures.add(pro);
			
		}
		} catch (Exception e) {
			return null;
		}
		return Structures;

	}
	public String delStructure (String delrapp) {
		String req="update Structure set delete=1 where id_Structure=?";
		try {
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1, delrapp);
			ps.executeUpdate();
			return "ok";
		} catch (Exception e) {
			return "Erreur"+e.getMessage();
		}
	}

	public Structure byNumStructure(String numrapp){
		String req="select from Structure where id_Structure=? and delet=1;";
		try {
			Structure rap =new Structure();
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1,numrapp);
			
			ResultSet rslt = ps.executeQuery();
			if(rslt.next()) {
			rap.setIdStructure(rslt.getString("id_Structure"));
				rap.setLibelleStructure(rslt.getString("libelle_structure"));
				
				
				}
			return rap;
			
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isExistNumRapprot(String numrapp){
		boolean exit =false;
		int i=0;
		String req="select * from Structure where id_Structure=? and delete=0";
		try {
			PreparedStatement ps=con.prepareStatement(req);
			ps.setString(1,numrapp);
			ResultSet rslt = ps.executeQuery();
			if(rslt.next()) {
				++i;
			}
			
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		if(i>0) {
			exit=true;
		}
		return exit;
	}

}
