/*
 * This file is generated by jOOQ.
 */
package models.tables.daos;


import java.time.LocalDate;
import java.util.List;

import models.tables.Activite;
import models.tables.records.ActiviteRecord;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ActiviteDao extends DAOImpl<ActiviteRecord, models.tables.pojos.Activite, String> {

    /**
     * Create a new ActiviteDao without any configuration
     */
    public ActiviteDao() {
        super(Activite.ACTIVITE, models.tables.pojos.Activite.class);
    }

    /**
     * Create a new ActiviteDao with an attached configuration
     */
    public ActiviteDao(Configuration configuration) {
        super(Activite.ACTIVITE, models.tables.pojos.Activite.class, configuration);
    }

    @Override
    public String getId(models.tables.pojos.Activite object) {
        return object.getNumActivite();
    }

    /**
     * Fetch records that have <code>num_activite BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfNumActivite(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.NUM_ACTIVITE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>num_activite IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByNumActivite(String... values) {
        return fetch(Activite.ACTIVITE.NUM_ACTIVITE, values);
    }

    /**
     * Fetch a unique record that has <code>num_activite = value</code>
     */
    public models.tables.pojos.Activite fetchOneByNumActivite(String value) {
        return fetchOne(Activite.ACTIVITE.NUM_ACTIVITE, value);
    }

    /**
     * Fetch records that have <code>libelle_activite BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfLibelleActivite(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.LIBELLE_ACTIVITE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>libelle_activite IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByLibelleActivite(String... values) {
        return fetch(Activite.ACTIVITE.LIBELLE_ACTIVITE, values);
    }

    /**
     * Fetch records that have <code>id_action BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfIdAction(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.ID_ACTION, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id_action IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByIdAction(String... values) {
        return fetch(Activite.ACTIVITE.ID_ACTION, values);
    }

    /**
     * Fetch records that have <code>delete BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfDelete(Byte lowerInclusive, Byte upperInclusive) {
        return fetchRange(Activite.ACTIVITE.DELETE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>delete IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByDelete(Byte... values) {
        return fetch(Activite.ACTIVITE.DELETE, values);
    }

    /**
     * Fetch records that have <code>creer_par BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfCreerPar(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.CREER_PAR, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>creer_par IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByCreerPar(String... values) {
        return fetch(Activite.ACTIVITE.CREER_PAR, values);
    }

    /**
     * Fetch records that have <code>cree_le BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfCreeLe(LocalDate lowerInclusive, LocalDate upperInclusive) {
        return fetchRange(Activite.ACTIVITE.CREE_LE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>cree_le IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByCreeLe(LocalDate... values) {
        return fetch(Activite.ACTIVITE.CREE_LE, values);
    }

    /**
     * Fetch records that have <code>id_structure BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfIdStructure(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.ID_STRUCTURE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id_structure IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByIdStructure(String... values) {
        return fetch(Activite.ACTIVITE.ID_STRUCTURE, values);
    }

    /**
     * Fetch records that have <code>type_activite BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfTypeActivite(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.TYPE_ACTIVITE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>type_activite IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByTypeActivite(String... values) {
        return fetch(Activite.ACTIVITE.TYPE_ACTIVITE, values);
    }

    /**
     * Fetch records that have <code>id_bailleur BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfIdBailleur(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.ID_BAILLEUR, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id_bailleur IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByIdBailleur(String... values) {
        return fetch(Activite.ACTIVITE.ID_BAILLEUR, values);
    }

    /**
     * Fetch records that have <code>id_fonction BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfIdFonction(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.ID_FONCTION, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id_fonction IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByIdFonction(String... values) {
        return fetch(Activite.ACTIVITE.ID_FONCTION, values);
    }

    /**
     * Fetch records that have <code>id_typefinancement BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfIdTypefinancement(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.ID_TYPEFINANCEMENT, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id_typefinancement IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByIdTypefinancement(String... values) {
        return fetch(Activite.ACTIVITE.ID_TYPEFINANCEMENT, values);
    }

    /**
     * Fetch records that have <code>livrable BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfLivrable(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.LIVRABLE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>livrable IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByLivrable(String... values) {
        return fetch(Activite.ACTIVITE.LIVRABLE, values);
    }

    /**
     * Fetch records that have <code>source_verification BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfSourceVerification(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.SOURCE_VERIFICATION, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>source_verification IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchBySourceVerification(String... values) {
        return fetch(Activite.ACTIVITE.SOURCE_VERIFICATION, values);
    }

    /**
     * Fetch records that have <code>statut BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<models.tables.pojos.Activite> fetchRangeOfStatut(String lowerInclusive, String upperInclusive) {
        return fetchRange(Activite.ACTIVITE.STATUT, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>statut IN (values)</code>
     */
    public List<models.tables.pojos.Activite> fetchByStatut(String... values) {
        return fetch(Activite.ACTIVITE.STATUT, values);
    }
}
