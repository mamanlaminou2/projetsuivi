/*
 * This file is generated by jOOQ.
 */
package models.tables;


import java.util.Arrays;
import java.util.List;

import models.Keys;
import models.Public;
import models.tables.records.ProfileRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row3;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Profile extends TableImpl<ProfileRecord> {

    private static final long serialVersionUID = 1912628901;

    /**
     * The reference instance of <code>public.profile</code>
     */
    public static final Profile PROFILE = new Profile();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ProfileRecord> getRecordType() {
        return ProfileRecord.class;
    }

    /**
     * The column <code>public.profile.num_profile</code>.
     */
    public final TableField<ProfileRecord, String> NUM_PROFILE = createField(DSL.name("num_profile"), org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false), this, "");

    /**
     * The column <code>public.profile.libelle_profile</code>.
     */
    public final TableField<ProfileRecord, String> LIBELLE_PROFILE = createField(DSL.name("libelle_profile"), org.jooq.impl.SQLDataType.VARCHAR, this, "");

    /**
     * The column <code>public.profile.delete</code>.
     */
    public final TableField<ProfileRecord, Byte> DELETE = createField(DSL.name("delete"), org.jooq.impl.SQLDataType.TINYINT.defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.TINYINT)), this, "");

    /**
     * Create a <code>public.profile</code> table reference
     */
    public Profile() {
        this(DSL.name("profile"), null);
    }

    /**
     * Create an aliased <code>public.profile</code> table reference
     */
    public Profile(String alias) {
        this(DSL.name(alias), PROFILE);
    }

    /**
     * Create an aliased <code>public.profile</code> table reference
     */
    public Profile(Name alias) {
        this(alias, PROFILE);
    }

    private Profile(Name alias, Table<ProfileRecord> aliased) {
        this(alias, aliased, null);
    }

    private Profile(Name alias, Table<ProfileRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    public <O extends Record> Profile(Table<O> child, ForeignKey<O, ProfileRecord> key) {
        super(child, key, PROFILE);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public UniqueKey<ProfileRecord> getPrimaryKey() {
        return Keys.PROFILE_PKEY;
    }

    @Override
    public List<UniqueKey<ProfileRecord>> getKeys() {
        return Arrays.<UniqueKey<ProfileRecord>>asList(Keys.PROFILE_PKEY);
    }

    @Override
    public Profile as(String alias) {
        return new Profile(DSL.name(alias), this);
    }

    @Override
    public Profile as(Name alias) {
        return new Profile(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Profile rename(String name) {
        return new Profile(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Profile rename(Name name) {
        return new Profile(name, null);
    }

    // -------------------------------------------------------------------------
    // Row3 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row3<String, String, Byte> fieldsRow() {
        return (Row3) super.fieldsRow();
    }
}
