/*
 * This file is generated by jOOQ.
 */
package models;


import java.util.Arrays;
import java.util.List;

import models.tables.Action;
import models.tables.Activite;
import models.tables.Affecter;
import models.tables.Agent;
import models.tables.Bailleur;
import models.tables.Fonction;
import models.tables.Natureeconomique;
import models.tables.Profile;
import models.tables.Programme;
import models.tables.Rapport;
import models.tables.Structure;
import models.tables.Tache;
import models.tables.Typefinancement;
import models.tables.User;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Public extends SchemaImpl {

    private static final long serialVersionUID = -1838439705;

    /**
     * The reference instance of <code>public</code>
     */
    public static final Public PUBLIC = new Public();

    /**
     * The table <code>public.action</code>.
     */
    public final Action ACTION = Action.ACTION;

    /**
     * The table <code>public.activite</code>.
     */
    public final Activite ACTIVITE = Activite.ACTIVITE;

    /**
     * The table <code>public.affecter</code>.
     */
    public final Affecter AFFECTER = Affecter.AFFECTER;

    /**
     * The table <code>public.agent</code>.
     */
    public final Agent AGENT = Agent.AGENT;

    /**
     * The table <code>public.bailleur</code>.
     */
    public final Bailleur BAILLEUR = Bailleur.BAILLEUR;

    /**
     * The table <code>public.fonction</code>.
     */
    public final Fonction FONCTION = Fonction.FONCTION;

    /**
     * The table <code>public.natureeconomique</code>.
     */
    public final Natureeconomique NATUREECONOMIQUE = Natureeconomique.NATUREECONOMIQUE;

    /**
     * The table <code>public.profile</code>.
     */
    public final Profile PROFILE = Profile.PROFILE;

    /**
     * The table <code>public.programme</code>.
     */
    public final Programme PROGRAMME = Programme.PROGRAMME;

    /**
     * The table <code>public.rapport</code>.
     */
    public final Rapport RAPPORT = Rapport.RAPPORT;

    /**
     * The table <code>public.structure</code>.
     */
    public final Structure STRUCTURE = Structure.STRUCTURE;

    /**
     * The table <code>public.tache</code>.
     */
    public final Tache TACHE = Tache.TACHE;

    /**
     * The table <code>public.typefinancement</code>.
     */
    public final Typefinancement TYPEFINANCEMENT = Typefinancement.TYPEFINANCEMENT;

    /**
     * The table <code>public.user</code>.
     */
    public final User USER = User.USER;

    /**
     * No further instances allowed
     */
    private Public() {
        super("public", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        return Arrays.<Table<?>>asList(
            Action.ACTION,
            Activite.ACTIVITE,
            Affecter.AFFECTER,
            Agent.AGENT,
            Bailleur.BAILLEUR,
            Fonction.FONCTION,
            Natureeconomique.NATUREECONOMIQUE,
            Profile.PROFILE,
            Programme.PROGRAMME,
            Rapport.RAPPORT,
            Structure.STRUCTURE,
            Tache.TACHE,
            Typefinancement.TYPEFINANCEMENT,
            User.USER);
    }
}
