/*
 * This file is generated by jOOQ.
 */
package models;


import models.tables.Action;
import models.tables.Activite;
import models.tables.Affecter;
import models.tables.Agent;
import models.tables.Bailleur;
import models.tables.Fonction;
import models.tables.Natureeconomique;
import models.tables.Profile;
import models.tables.Programme;
import models.tables.Rapport;
import models.tables.Structure;
import models.tables.Tache;
import models.tables.Typefinancement;
import models.tables.User;


/**
 * Convenience access to all tables in public
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>public.action</code>.
     */
    public static final Action ACTION = Action.ACTION;

    /**
     * The table <code>public.activite</code>.
     */
    public static final Activite ACTIVITE = Activite.ACTIVITE;

    /**
     * The table <code>public.affecter</code>.
     */
    public static final Affecter AFFECTER = Affecter.AFFECTER;

    /**
     * The table <code>public.agent</code>.
     */
    public static final Agent AGENT = Agent.AGENT;

    /**
     * The table <code>public.bailleur</code>.
     */
    public static final Bailleur BAILLEUR = Bailleur.BAILLEUR;

    /**
     * The table <code>public.fonction</code>.
     */
    public static final Fonction FONCTION = Fonction.FONCTION;

    /**
     * The table <code>public.natureeconomique</code>.
     */
    public static final Natureeconomique NATUREECONOMIQUE = Natureeconomique.NATUREECONOMIQUE;

    /**
     * The table <code>public.profile</code>.
     */
    public static final Profile PROFILE = Profile.PROFILE;

    /**
     * The table <code>public.programme</code>.
     */
    public static final Programme PROGRAMME = Programme.PROGRAMME;

    /**
     * The table <code>public.rapport</code>.
     */
    public static final Rapport RAPPORT = Rapport.RAPPORT;

    /**
     * The table <code>public.structure</code>.
     */
    public static final Structure STRUCTURE = Structure.STRUCTURE;

    /**
     * The table <code>public.tache</code>.
     */
    public static final Tache TACHE = Tache.TACHE;

    /**
     * The table <code>public.typefinancement</code>.
     */
    public static final Typefinancement TYPEFINANCEMENT = Typefinancement.TYPEFINANCEMENT;

    /**
     * The table <code>public.user</code>.
     */
    public static final User USER = User.USER;
}
